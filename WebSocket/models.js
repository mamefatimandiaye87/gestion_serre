const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    Temperature: {type: String},
    HumidityA: {type: String},
    Luminosite: {type: String},
    HumidityS: {type: String},
    Date: {type: String},
    Heure: {type: String}
},
{
    collection: 'donnees'
});

module.exports = mongoose.model('donnees', schema);