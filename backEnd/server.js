const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
// Express APIs
const api = require('./routes/auth.routes')

mongoose


.connect('mongodb+srv://isacadel98:traore@serreauto.2c6trwo.mongodb.net/?retryWrites=true&w=majority')
  .then((x) => {
    console.log(`Connected to Mongo! Database name: "${x.connections[0].name}"`)
  })
  .catch((err) => {
    console.error('Error connecting to mongo', err.reason)
  })

// Express settings
const app = express()
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
)
app.use(cors())

// Serve static resources

 app.use('/api', api)

// Error favicon.ico
 app.get('/favicon.ico', (req, res) => res.status(204))

// Define PORT
 const port = process.env.PORT || 4000
 const server = app.listen(port, () => {
  console.log('connecté au port ' + port)
})
// Express error handling
app.use((req, res, next) => {
  setImmediate(() => {
    next(new Error('Something went wrong'))
  })
})

app.use(function (err, req, res, next) {
  console.error(err.message)
  if (!err.statusCode) err.statusCode = 500
  res.status(err.statusCode).send(err.message)
})







/*
const mongoose = require('mongoose');

const connectDB = async () => {
    const conn = await mongoose.connect('mongodb+srv://mamefatiman05:toto@cluster0.glwvxpm.mongodb.net/?retryWrites=true&w=majority')

    console.log(`MongoDB connected : ${conn.connection.host}`)
}

connectDB()  */
