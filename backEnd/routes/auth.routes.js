const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const router = express.Router()
const userSchema = require('../models/User')
const authorize = require('../middlewares/auth')
const { check, validationResult } = require('express-validator')
mongoose = require('mongoose')
multer = require('multer')


// Inscription
router.post('/register-user',
  [
    check('nom').not().isEmpty(),
    check('prenom').not().isEmpty(),
    check('email', 'Email is required').not().isEmpty(),
    check('password', 'Password should be between 8 to 16 characters long')
      .not()
      .isEmpty()
      .isLength({ min: 8, max: 16 }),
  ],
  (req, res, next) => {
    const errors = validationResult(req)
    console.log(req.body)

      bcrypt.hash(req.body.password, 10).then((hash) => {

        const url = req.protocol + '://' + req.get('host')
        const user = new userSchema({
          prenom: req.body.prenom,
          nom: req.body.nom,
          email: req.body.email,
          password: hash,
        })
        user
          .save()
          .then((response) => {
            console.log(response);
            res.status(201).json({
              message: 'Inscription réussie !',
              result: response,
            })
          })
          .catch((error) => {
            res.status(409).json({
              error: error.message.split("email:")[1],
            })
          })
      })
  },
)
/* recuperer tt les utilisateurs */
router.route('/').get((req, res, next) => {
  userSchema.find((error, response)=> {
    if (error) {
      return next(error)
    } else {
      return res.status(200).json(response)
    }
  })
})

// Connexion
router.post('/signin', (req, res, next) => {
  let getUser
  userSchema
    .findOne({
      email: req.body.email,
    })
    // Verifier si l'utilisateur existe
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          message: 'Compte non existant !',
        })
      }
    else  if (!response) {
        return res.status(401).json({
          message: 'Le mot de passe est incorrect !',
        })
      }

      getUser = user
      return bcrypt.compare(req.body.password, user.password)
    })
    .then((response) => {
      if (!response) {
        return res.status(401).json({
          message: 'Le mot de passe est incorrect !',
        })
      }else if(getUser.etat == true){
        return res.status(401).json({
          message: 'Le compte est désactivé !' ,
        })
      }
      let jwtToken = jwt.sign(
        {
          email: getUser.email,
          userId: getUser._id,
        },
        'longer-secret-is-better', 
        {
          expiresIn: '1h',
        },
      )
      res.status(200).json({
        token: jwtToken,
        expiresIn: 3600,
        _id: getUser._id,
      })
    })
    .catch((err) => {
      return res.status(401).json({
        message: 'Authentication failed',
      })
    })
})



// Recuperez un utilisateur
router.route('/read-user/:id').get((req, res) => {
  userSchema.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

// Modification mot de passe
router.route('/updatepassword/:id').patch( async(req, res) => {
  try {
    let { ancienpassword, newpassword } = req.body;
    console.log(req.body);
  const id = req.params.id;
  const updatedData = req.body;
  const options = { new: true };
   let user =await userSchema.findById({"_id": req.params.id}) 
 console.log(bcrypt.compare(ancienpassword, user.newpassword));
 const comp = await bcrypt.compare(ancienpassword, user.password);
  if(!user){
    res.status(400).json({message: "veuillez saisir votre actuel mot de passe!"})
    return;
  }

     if(updatedData.ancienpassword) {
      console.log(user);
     

        if(comp){
            const hash = await bcrypt.hash(newpassword, 10);
              updatedData.password = hash;
              const result = await userSchema.findByIdAndUpdate(
              id, updatedData, options
              );
            return res.send(result);
        }

        return res.send('no corres');
       
  }
  else{
    const result = await userSchema.findByIdAndUpdate(
          id, updatedData, options
      )
      return res.send(result)
      
  }
  return bcrypt.compare( user.newpassword,);
}
  catch (error) {
      res.status(400).json({ message: error.message })
  }
  })



module.exports = router
