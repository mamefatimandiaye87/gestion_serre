import prof from '../histo.json';
import { AuthService } from 'src/app/services/auth.service';
import { Iot } from 'src/app/models/iot';
import { Component,OnInit} from '@angular/core';


import { Socket } from 'ngx-socket-io'; 
export interface Histo{
  Date: string
 Temperature: string
 HumiditeA: string
 HumiditeS: string
 Luminosite: string 
}

@Component({
 selector: 'app-historique',
 templateUrl: './historique.component.html',
 styleUrls: ['./historique.component.css'],
 
})
export class HistoriqueComponent implements OnInit {
[x: string]: any;
 filterTerm!: string;
 profilhis: any=[];
 totalLenght: any;
 page : number=1;
 itemsPerPage:number=6;
 Date!: string;
 Temperature!: string;
 HumidityA!: string;
 HumiditeS!: string;
 Luminosite!: string;
 socket:any;
;/*   histo!: Iot[];  */
public histo!: any[];
constructor(private Socket: AuthService,){

 }
/*
 donne8h: any; donne12h: any; donne19h: any
 constructor(private socket: Socket,
   public authService: AuthService){

this.socket.connect();
this.socket.on('temperature', (temperature: number) => {
this.temperature = temperature; 
if (temperature > 30) { // Afficher le ventillateur allumé lorsque la temperature est supérieur a 30
this.on = true
this.off = false
}

});
this.socket.on('humidity', (humidity: number) => {
this.humidity = humidity;
});

this.socket.on('status', (status: string) => {
console.log(`Status: ${status}`);
});
}
*/

 ngOnInit(): void {
   this.Socket.gethisto().subscribe(( data:any)=>{
     this.histo=data
     console.log(this.histo)
   });
  this.historiques=prof;
  console.log(this.historiques)
  
  this.jour = new Date().getDate()
  this.mois = new Date().getMonth()+1
  if (this.jour < 10) {
    this.jour = '0' + this.jour
  }
  if (this.mois < 10) {
    this.mois = '0' + this.mois
  }
 
  this.dateNow  = this.jour + '/' + this.mois + '/' + new Date().getFullYear()
  this.last_week = this.jour - 7 + '/' + this.mois + '/' + new Date().getFullYear(); 
  console.log(this.last_week);

  this.Socket.gethisto().subscribe(
    (      data: unknown) => {
    // this.histo = data as unknown as Iot[]; // Stoke tous les données de la bdd
       this.donne8h = this.histo.filter((e:any)=> e.Heure == '08:00:00' && e.Date == this.dateNow) // Recupere les données du jour à 8H
       /* this.donne12h = this.histo.filter((e:any)=> e.Heure == '12:00:00' && e.Date == this.dateNow)
       this.donne19h = this.histo.filter((e:any)=> e.Heure == '19:00:00' && e.Date == this.dateNow) */
       this.historique = this.histo.filter((e:any)=> e.Date != this.dateNow); console.log(this.historique); // Recupere l'historique de la semaine
       
       this.hist8h = this.historique.filter((h:any)=> h.Heure == '08:00:00') // Recupére l'historique à 8H
    /*    this.hist12h = this.historique.filter((h:any)=> h.Heure == '12:00:00')
       this.hist19h = this.historique.filter((h:any)=> h.Heure == '19:00:00') */

 
   }
 )
 }

 search(e:any) {
   console.log(e.target.value)
   this.histo=this.histo.filter((el:any)=>{
     return el.Date.toLowerCase().includes(e.target.value.toLowerCase())

   })
     /* recherche */
 }

 /*  historiques: Histo[]= prof  */
}


