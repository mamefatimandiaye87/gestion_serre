

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
  import { Ng2SearchPipeModule } from 'ng2-search-filter'; 
import { NgxPaginationModule } from 'ngx-pagination'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './shared/main/main.component';
import { EditPasswordComponent } from './shared/edit-password/edit-password.component';
import { ParamArrosageComponent } from './shared/param-arrosage/param-arrosage.component';
import { HistoriqueComponent } from './historique/historique.component'






@NgModule({
  declarations: [	
    AppComponent, 
    LoginComponent, 
    DashboardComponent,
     SidebarComponent, 
     MainComponent,  
     EditPasswordComponent,
      ParamArrosageComponent,
       HistoriqueComponent,
  
      
   ],

  imports: [BrowserModule,
    ReactiveFormsModule,
    FormsModule, 
    AppRoutingModule, 
    
    NgxPaginationModule,
     Ng2SearchPipeModule,
     HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule {}
