import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParamArrosageComponent } from './param-arrosage.component';

describe('ParamArrosageComponent', () => {
  let component: ParamArrosageComponent;
  let fixture: ComponentFixture<ParamArrosageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParamArrosageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ParamArrosageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
