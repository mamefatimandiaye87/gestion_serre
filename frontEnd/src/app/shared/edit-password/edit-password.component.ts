import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/MustMatch';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.css']
})
export class EditPasswordComponent {
  //*output tranfert donné enfant vers parent///
  @Output() closed = new EventEmitter()
  @Input()
  isOpen = false
  user: any

  open() {
    this.isOpen = true
  }

  close() {
    this.isOpen = false
    this.closed.emit()
  }

  form!: FormGroup
  submitted = false
  Users: any = []
  /* errMsg:any = false; */
   errMsg!:string; 
  formGroup: any;
  updateForm: any;
  /* pass!: string */

  constructor(
    private formBuilder: FormBuilder,public authService: AuthService)
    {
      this.updateForm = this.formBuilder.group({
        ancienpassword:['',[Validators.required,Validators.minLength(6)]],
        newpassword:['',[Validators.required,Validators.minLength(6)]],
        passwordConfirm: ['', Validators.required],
         },
    { validator: MustMatch('newpassword', 'passwordConfirm') });

  }
  ngOnInit(): void {
    this.authService.GetUsers().subscribe((data) => {
      this.user = data
      this.Users = this.user.filter((e: any) => e.etat == true)
      console.log(this.Users)
    })
  }


  updatepass(){
   /*  let id = localStorage.getItem('id')?.replaceAll('"', ''); */
    const id =  this.updateForm.value.id; 
      const user ={
        ancienpassword: this.updateForm.value.ancienpassword,
        newpassword: this.updateForm.value.newpassword,
        passwordConfirm: this.updateForm.value.passwordConfirm

   }
   this.submitted = true;
   if(this.updateForm.invalid){
    console.log(this.updateForm.errors);
     return; 
   }
   // retourne a la page deconnection apres le popup modification reussi
   return this.authService.updatepass(localStorage.getItem('id'),user).subscribe((data)=>{
    this.ngOnInit(); 
     
    Swal.fire({
     
      position: 'center',
      icon: 'success',
      title: 'Modification  mot de passe réussi !',
      showConfirmButton: false,
      timer: 1500
    });
   this.authService.doLogout()
   },
   /* error => {
    this.errMsg = false
    setTimeout(() => { this.errMsg = true }, 2000);
  } */
    /* (err)=>{
       this.pass="mot de passe actuel est incorrect ";
   })  */ 

    error => {
    this.errMsg = error.error.message
    console.log(error.error.message)
  } )
}}
