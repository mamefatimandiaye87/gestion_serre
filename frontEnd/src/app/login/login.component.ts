import { IotService } from './../services/iot.service';
import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { AuthService } from '../services/auth.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
 // [x: string]: any
  registerForm!: FormGroup
  donne: any = {}
  errMsg:any

  submitted = false
  loginError: string | null=null
  /* on a injecter formbuilder de type formbuilder */

  /* on a injecter formbuilder de type formbuilder */
  constructor(private formBuilder: FormBuilder,private authService: AuthService, private router: Router ,private iotservice:IotService) {
    this.registerForm = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{3,4}$'),
        ],
      ],
      password: ['', [Validators.required, Validators.minLength(8)]],
    })

    this.iotservice.uid().subscribe((data:any)=> {
      localStorage.setItem('access_token', data.token);
      localStorage.setItem('id', data._id);
      this.router.navigate(['/dashboard' ]);
      console.log(data);
    })

  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.submitted = true

    if (this.registerForm.invalid) {
      return
    }
    this.authService.signIn(this.registerForm.value).subscribe((res: any) => {
      localStorage.setItem('access_token', res.token);
      localStorage.setItem('id', res._id);
      this.router.navigate(['/dashboard' ]);

   /*    this.authService.getUserProfile(res._id).subscribe((res: { msg: { _id: string } }) => {
        this.authService.currentUser = res;
        this.router.navigate(['user-profile/' + res.msg._id]);
      }) */
    }, // Intercepter les messages d'erreurs du serveur
    error => {
      this.errMsg = error.error.message
      console.log(error.error.message)
    }
   )}
    /* this.authService.login(this.registerForm.value).subscribe({

      next: (response: { access_token: string; email: string; }) => {
        localStorage.setItem('access_token', response.access_token);
        localStorage.setItem('email', response.email);
        this.router.navigateByUrl('/dashboard');
      },
      error: (err: { error: { message: string | null; }; }) => {
        this.loginError = err.error.message
        setTimeout(() => {
          this.loginError = null;
        }, 5000)
      },
      complete: () => console.log('complete')
    }) */


  }
